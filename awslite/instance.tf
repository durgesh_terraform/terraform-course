resource "aws_instance" "t2micro" {
  # AMI
  ami = var.AWS_AMIS[var.AWS_REGION]
  
  # Instance Type
  instance_type = "t2.micro"

  # Availability Zone  
  availability_zone = "ap-south-1a"

  # Public Key
  key_name = var.AWS_PUBLIC_KEY

  # User Data
  user_data = data.template_cloudinit_config.cloudinit-example.rendered
  
}

# EBS Volume
resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "ap-south-1a"
  size = 5
  type = "gp2"
  tags = {
    Name = "extra volume data"
  }
}

# EBS Volume Attachment
resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "/dev/xvdh"
  volume_id = aws_ebs_volume.ebs-volume-1.id
  instance_id = aws_instance.t2micro.id
}

output "ip" {
 value = aws_instance.t2micro.public_ip
}
