#AWS Region
variable "AWS_REGION" {
  type = string
}

#AWS Key
variable "AWS_KEY" {
  type = string
}

# AWS Secret
variable "AWS_SECRET" {
  type = string
}

# Map of AMIs
variable "AWS_AMIS" {
  type = map(string)
  default = {
    "ap-south-1" = "ami-0c1a7f89451184c8b"
  }
}

# Public Key Name
variable "AWS_PUBLIC_KEY" {
  type = string
  default = "terraformkey"
}

# Path to private key for ssh connection
variable "PRIVATE_KEY" {
  type = string
  default = "./keys/terraformkey"
}

# User for ssh connection
variable "USER" {
  type = string
  default = "ubuntu"
}

# Device Name for EBS Volume
variable "INSTANCE_DEVICE_NAME" {
  type = string
  default = "/dev/xvdh"
}
