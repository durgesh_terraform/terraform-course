output "instance" {
  value = aws_instance.t2micro.public_ip
}

output "rds" {
  value = aws_db_instance.mariadb.endpoint
}
