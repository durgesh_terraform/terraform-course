resource "aws_instance" "t2micro" {
  # AMI
  ami = var.AWS_AMIS[var.AWS_REGION]
  
  # Instance Type
  instance_type = "t2.micro"

  # VPC Subnet
  subnet_id = aws_subnet.main-public-1.id

  # Public Key
  key_name = var.AWS_PUBLIC_KEY

  # Security Group
  vpc_security_group_ids = [aws_security_group.allow-ssh.id] 
}
