resource "aws_instance" "t2micro" {
  ami = var.AWS_AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  
  # PUBLIC KEY
  key_name = var.AWS_PUBLIC_KEY

  # VPC
  subnet_id = aws_subnet.main-public-1.id

  # SSH Security Group
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  # ROLE
  iam_instance_profile = aws_iam_instance_profile.s3-mybucket-role-instanceprofile.name

}



