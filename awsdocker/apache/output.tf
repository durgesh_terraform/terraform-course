output "apachedocker-repo-URL" {
  value = aws_ecr_repository.apache.repository_url
}

output "elb" {
  value = aws_elb.apache-elb.dns_name
}