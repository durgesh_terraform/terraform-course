variable "AWS_REGION" {
  type = string
  default = "ap-south-1"
}

#AWS Key
variable "AWS_KEY" {
  type = string
}

# AWS Secret
variable "AWS_SECRET" {
  type = string
}

variable "ECS_INSTANCE_TYPE" {
  type = string
  default = "t2.micro"
}

variable "ECS_AMIS" {
  type = map(string)
  default = {
    ap-south-1 = "ami-087632d3a5a48acf4"
  }
}

variable "AWS_PUBLIC_KEY" {
  type = string
  default = "terraformkey"
}