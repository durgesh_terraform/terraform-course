resource "aws_instance" "t2micro" {
  ami = var.AWS_AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name = var.AWS_PUBLIC_KEY

  subnet_id = aws_subnet.main-public-1.id

  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  provisioner "local-exec" {
    command = "echo t2micro ansible_hostname=${aws_instance.t2micro.public_ip} ansible_connection=ssh ansible_user=${var.USER} ansible_ssh_private_key_file=${var.PRIVATE_KEY} > inventory"
  }

}

# EBS Volume
resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "ap-south-1a"
  size = 5
  type = "gp2"
  tags = {
    Name = "extra volume data"
  }
}

# EBS Volume Attachment
resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "/dev/xvdh"
  volume_id = aws_ebs_volume.ebs-volume-1.id
  instance_id = aws_instance.t2micro.id
}

output "ip" {
 value = aws_instance.t2micro.public_ip
}

  # Copy script
  #provisioner "file" {
    #source = "script.sh"
    #destination = "/home/ubuntu/script.sh"
  #}

  # Execute script
  #provisioner "remote-exec" {
    #inline = [
      #"chmod +x ./script.sh",
      #"sudo ./scipt.sh",
    #]
  #}

  # SSH Connection
  #connection {
    #host = coalesce(self.public_ip, self.private_ip)
    #type = "ssh"
    #user = var.USER
    #private_key = file(var.PRIVATE_KEY)
  #}


