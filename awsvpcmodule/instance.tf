data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
      name = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
      name = "virtualization-type"
      values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "t2micro" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  subnet_id = var.ENV == "prod" ? module.vpc-prod.public_subnets[0] : module.vpc-dev.public_subnets[0]

  vpc_security_group_ids = [var.ENV == "prod" ? aws_security_group.allow-ssh-prod.id : aws_security_group.allow-ssh-dev.id]

  key_name = var.AWS_PUBLIC_KEY
}